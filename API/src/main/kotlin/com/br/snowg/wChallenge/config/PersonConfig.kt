package com.br.snowg.wChallenge.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

private const val HIGH_LEVEL = "high"
private const val MEDIUM_LEVEL = "medium"

@Component
data class PersonConfig(
        @Value("\${$HIGH_LEVEL}") val high: Array<String>,
        @Value("\${$MEDIUM_LEVEL}") val medium: Array<String>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is PersonConfig) return false

        if (!high.contentEquals(other.high)) return false
        if (!medium.contentEquals(other.medium)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = high.contentHashCode()
        result = 31 * result + medium.contentHashCode()
        return result
    }
}