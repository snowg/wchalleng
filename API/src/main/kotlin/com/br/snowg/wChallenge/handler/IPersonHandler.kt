package com.br.snowg.wChallenge.handler

import com.br.snowg.wChallenge.view.InputPerson
import com.br.snowg.wChallenge.view.ViewPerson

interface IPersonHandler {
    fun savePerson(person: InputPerson): ViewPerson
    fun getPersonByID(personID: String): ViewPerson
}