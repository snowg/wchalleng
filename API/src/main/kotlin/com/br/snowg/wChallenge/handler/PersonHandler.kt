package com.br.snowg.wChallenge.handler

import com.br.snowg.wChallenge.config.PersonConfig
import com.br.snowg.wChallenge.repository.IPersonRepository
import com.br.snowg.wChallenge.service.PersonService
import com.br.snowg.wChallenge.view.InputPerson
import com.br.snowg.wChallenge.view.ViewPerson
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PersonHandler: IPersonHandler{

    @Autowired
    private lateinit var repo: IPersonRepository

    @Autowired
    private lateinit var personService: PersonService



    override fun savePerson(person: InputPerson): ViewPerson {
        val encryptedPerson = personService.encryptPerson(person)
        return repo.save(encryptedPerson).toView()
    }

    override fun getPersonByID(personID: String): ViewPerson {
        return personService.decryptPerson(repo.findById(personID).get())
    }
}