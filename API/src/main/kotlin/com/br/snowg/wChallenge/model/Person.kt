package com.br.snowg.wChallenge.model

import com.br.snowg.wChallenge.converters.AgeConverter.Companion.intToBand
import com.br.snowg.wChallenge.view.ViewPerson
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "persons")
data class Person(
        @Id
        var id: ObjectId?,
        var name: ByteArray,
        var email: ByteArray,
        var cpf: ByteArray,
        var address: ByteArray,
        var age: Int?
){
        fun toView(): ViewPerson{
                return ViewPerson(
                        id = this.id.toString(),
                        name = String(this.name),
                        email = String(this.email),
                        cpf = String(this.cpf),
                        address = String(this.address),
                        age = intToBand(this.age ?: 0)
                )
        }

        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Person

                if (id != other.id) return false
                if (!name.contentEquals(other.name)) return false
                if (!email.contentEquals(other.email)) return false
                if (!cpf.contentEquals(other.cpf)) return false
                if (!address.contentEquals(other.address)) return false
                if (age != other.age) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id?.hashCode() ?: 0
                result = 31 * result + name.contentHashCode()
                result = 31 * result + email.contentHashCode()
                result = 31 * result + cpf.contentHashCode()
                result = 31 * result + address.contentHashCode()
                result = 31 * result + (age ?: 0)
                return result
        }
}