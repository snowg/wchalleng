package com.br.snowg.wChallenge.service

import org.springframework.stereotype.Service
import java.nio.charset.Charset
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

@Service
class CriptService {

    // Criptografia simetrica utilizando AES

    private val key = "0123456789abcdef"
    private val iv = "AAAAAAAAAAAAAAAA"


    fun encrypt(text: String): String {
        val encrypt = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE")
        val encryptKey = SecretKeySpec(key.toByteArray(Charset.forName("UTF-8")),"AES")
        encrypt.init(Cipher.ENCRYPT_MODE,encryptKey,IvParameterSpec(iv.toByteArray(Charset.forName("UTF-8"))))
        //val byteText = text.toByteArray().toTypedArray()
        val bytes = encrypt.doFinal(text.toByteArray(Charset.forName("UTF-8")))
        return String(bytes)
    }

    fun decrypt(text: String): String{
        val encrypt = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE")
        val encryptKey = SecretKeySpec(key.toByteArray(Charset.forName("UTF-8")),"AES")
        encrypt.init(Cipher.DECRYPT_MODE,encryptKey,IvParameterSpec(iv.toByteArray(Charset.forName("UTF-8"))))
        val bytes = encrypt.doFinal(text.toByteArray(Charset.forName("UTF-8")))
        return String(bytes)
    }

}