package com.br.snowg.wChallenge.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.security.KeyPairGenerator
import java.io.*
import java.nio.charset.Charset
import java.security.PrivateKey
import java.security.PublicKey
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec


@Service
class RSAService {

    @Autowired
    private lateinit var aesService: AESService

    private val algorithm = "RSA"
    //private val publicKeyPath = "C:/keys/public.key"
    private val publicKeyPath = "./keys/public.key"
    //private val privateKeyPath = "C:/keys/private.key"
    private val privateKeyPath = "./keys/private.key"
    //private val key = "0123456789abcdef"
    private lateinit var privateKey: PrivateKey
    private lateinit var publicKey: PublicKey

    init {

        if(!check()){
            genKey()
        }

        readKey()

    }

    private fun check(): Boolean {
        val publicKeyFile = File(publicKeyPath)
        val privateKeyFile = File(privateKeyPath)

        if(publicKeyFile.exists().and(privateKeyFile.exists())){
            return true
        }

        return false
    }


    private fun genKey(){
        val keyGen = KeyPairGenerator.getInstance(algorithm)
        keyGen.initialize(1024)
        val key = keyGen.genKeyPair()

        val publicKeyFile = File(publicKeyPath)
        val privateKeyFile = File(privateKeyPath)

        if(publicKeyFile.parentFile != null){
            publicKeyFile.parentFile.mkdirs()
        }

        publicKeyFile.createNewFile()

        ObjectOutputStream(FileOutputStream(publicKeyFile)).use {
            it.writeObject(key.public)
            it.close()
        }

        ObjectOutputStream(FileOutputStream(privateKeyFile)).use {
            it.writeObject(key.private)
            it.close()
        }
    }

    fun encrypt(text: String): ByteArray {
        var cipherText: ByteArray? = null
        try {
            val byteCipherText = aesService.encrypt(text)

            val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
            cipher.init(Cipher.ENCRYPT_MODE, publicKey)
            cipherText = cipher.doFinal(byteCipherText)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return cipherText!!
    }

    fun decrypt(text: ByteArray): String {
        var decryptedBytes: ByteArray? = null
        var decryptedText = ""
        try {
            val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
            cipher.init(Cipher.DECRYPT_MODE, privateKey)
            decryptedBytes = cipher.doFinal(text)
            decryptedText = String(aesService.decrypt(decryptedBytes))

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return decryptedText
    }

    private fun readKey(){
        val publicKeyInput = ObjectInputStream(FileInputStream(publicKeyPath))
        this.publicKey = publicKeyInput.readObject() as PublicKey

        val privateKeyInput = ObjectInputStream(FileInputStream(privateKeyPath))
        this.privateKey = privateKeyInput.readObject() as PrivateKey
    }
}