package com.br.snowg.wChallenge

import com.br.snowg.wChallenge.service.RSAService
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WChallengeApplication

fun main(args: Array<String>) {
	runApplication<WChallengeApplication>(*args)
}
