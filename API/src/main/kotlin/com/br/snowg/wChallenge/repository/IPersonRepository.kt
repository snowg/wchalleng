package com.br.snowg.wChallenge.repository

import com.br.snowg.wChallenge.model.Person
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface IPersonRepository: MongoRepository<Person,String>