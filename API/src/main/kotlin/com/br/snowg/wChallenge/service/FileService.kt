package com.br.snowg.wChallenge.service

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class FileService {

    private val filesBaseOPath: String = "~\\wchallenge"


    fun checkExist(path: String? = null): Boolean{
        val paths = Paths.get(path)
        return Files.exists(paths)
    }

    fun createFile(file: String): File{
        val file = File(filesBaseOPath + "\\" + file)
        file.createNewFile()
        return file
    }

}