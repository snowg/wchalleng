package com.br.snowg.wChallenge.view

data class InputPerson(
        val id: String? = null,
        val name: String? = null,
        val email: String? = null,
        val cpf: String? = null,
        val address: String? = null,
        val age: Int? = null
)