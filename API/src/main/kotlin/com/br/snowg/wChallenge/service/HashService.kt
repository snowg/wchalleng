package com.br.snowg.wChallenge.service

import org.springframework.stereotype.Service
import java.nio.charset.StandardCharsets
import java.security.MessageDigest

@Service
class HashService {
    fun encodeSHA256(text: String): ByteArray {
        val digest = MessageDigest.getInstance("SHA-256")
        return digest.digest(text.toByteArray(StandardCharsets.UTF_8))
    }
}