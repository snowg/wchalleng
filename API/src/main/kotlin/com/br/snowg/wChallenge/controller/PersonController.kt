package com.br.snowg.wChallenge.controller

import com.br.snowg.wChallenge.handler.IPersonHandler
import com.br.snowg.wChallenge.view.InputPerson
import com.br.snowg.wChallenge.view.ViewPerson
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class PersonController {

    @Autowired
    lateinit var handler: IPersonHandler

    @PostMapping(value = ["/person"])
    fun save(@RequestBody person: InputPerson): ResponseEntity<*>{
        return ResponseEntity.ok(handler.savePerson(person))
    }

    @GetMapping(value = ["/person/{personId}"])
    fun getpersonbyid(@PathVariable(value = "personId")personId : String): ResponseEntity<*>{
        return ResponseEntity.ok(handler.getPersonByID(personId))
    }
}