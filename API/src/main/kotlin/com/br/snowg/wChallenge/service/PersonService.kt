package com.br.snowg.wChallenge.service

import com.br.snowg.wChallenge.config.PersonConfig
import com.br.snowg.wChallenge.converters.AgeConverter.Companion.intToBand
import com.br.snowg.wChallenge.model.Person
import com.br.snowg.wChallenge.view.InputPerson
import com.br.snowg.wChallenge.view.ViewPerson
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.swing.text.View

@Service
class PersonService {

    @Autowired
    lateinit var personConfig: PersonConfig

    @Autowired
    lateinit var encryptService: RSAService

    @Autowired
    lateinit var hashService: HashService

    fun encryptPerson(person: InputPerson): Person{
        val nvPerson = Person(null,
                person.name?.toByteArray() ?: ByteArray(0),
                person.email?.toByteArray() ?: ByteArray(0),
                person.cpf?.toByteArray() ?: ByteArray(0),
                person.address?.toByteArray() ?: ByteArray(0),
                person.age)

        if(personConfig.medium.contains("name")) { nvPerson.name = encryptService.encrypt(person.name ?: "") }
        if(personConfig.medium.contains("email")) { nvPerson.email = encryptService.encrypt(person.email ?: "") }
        if(personConfig.medium.contains("cpf")) { nvPerson.cpf = encryptService.encrypt(person.cpf ?: "") }
        if(personConfig.medium.contains("address")) { nvPerson.address = encryptService.encrypt(person.address ?: "") }

        if(personConfig.high.contains("name")) { nvPerson.name = hashService.encodeSHA256(person.name ?: "") }
        if(personConfig.high.contains("email")) { nvPerson.email = hashService.encodeSHA256(person.email ?: "") }
        if(personConfig.high.contains("cpf")) { nvPerson.cpf = hashService.encodeSHA256(person.cpf ?: "") }
        if(personConfig.high.contains("address")) { nvPerson.address = hashService.encodeSHA256(person.address ?: "") }

        return nvPerson
    }

    fun decryptPerson(person: Person): ViewPerson {
        val newPerson = ViewPerson(
                id = person.id.toString(),
                age = intToBand(person.age ?: 0))
        if(personConfig.medium.contains("name") ) { newPerson.name = encryptService.decrypt(person.name) }
        if(personConfig.medium.contains("email") ) { newPerson.email = encryptService.decrypt(person.email) }
        if(personConfig.medium.contains("cpf") ) { newPerson.cpf = encryptService.decrypt(person.cpf) }
        if(personConfig.medium.contains("address") ) { newPerson.address = encryptService.decrypt(person.address) }


        if(!personConfig.medium.contains("name") ){ newPerson.name = String(person.name) }
        if(!personConfig.medium.contains("email")) { newPerson.email = String(person.email) }
        if(!personConfig.medium.contains("cpf") ) { newPerson.cpf = String(person.cpf) }
        if(!personConfig.medium.contains("address") ) { newPerson.address = String(person.address) }

        return newPerson
    }
}