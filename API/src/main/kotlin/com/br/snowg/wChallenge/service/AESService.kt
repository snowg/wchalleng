package com.br.snowg.wChallenge.service

import org.springframework.stereotype.Service
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

@Service
class AESService {
    private val key = "0123456789abcdef"

    fun encrypt(text: String): ByteArray{
        val aesCipher = Cipher.getInstance("AES")
        val encryptKey = SecretKeySpec(key.toByteArray(StandardCharsets.UTF_8),"AES")
        aesCipher.init(Cipher.ENCRYPT_MODE, encryptKey)
        return aesCipher.doFinal(text.toByteArray(StandardCharsets.UTF_8))
    }

    fun decrypt(text: ByteArray): ByteArray {
        val originalKey = SecretKeySpec(key.toByteArray(StandardCharsets.UTF_8) , 0, key.length, "AES")
        val aesCipher = Cipher.getInstance("AES")
        aesCipher.init(Cipher.DECRYPT_MODE, originalKey)
        return aesCipher.doFinal(text)
    }
}