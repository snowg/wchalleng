package com.br.snowg.wChallenge.view

import com.br.snowg.wChallenge.model.Person
import org.bson.types.ObjectId

data class ViewPerson(
        var id: String? = null,
        var name: String? = null,
        var email: String? = null,
        var cpf: String? = null,
        var address: String? = null,
        var age: String?
)