package com.br.snowg.wChallenge.converters

class AgeConverter {
    companion object{
        fun intToBand(age: Int): String{
            return getMin(age).toString() + "-" + getMax(age).toString()
        }
        private fun getMin(age: Int): Int {
            var minAge = age
            while (minAge % 10 != 0)
            {
                minAge--
            }
            return minAge
        }

        private fun getMax(age: Int): Int {
            var maxAge = age
            while (maxAge % 10 != 0)
            {
                maxAge++
            }
            return maxAge
        }
    }


}