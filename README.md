# WChalleng

Projeto desenvolvido para estudo de tecnologias de criptografia.

Foram utilizadas os seguintes tipos de criptografia:
- RSA (Criptografia Assimetrica)
- AES (Criptografia Simetrica)
- SHA-256 (Hash)

É aplicado uma das criptografias para os campos do modelo "Person" de acordo com os dados fornecidos no aplicattion.properties, sendo:
* high: Arquivos criptografados com hash
* medium: Arquivos criptografados com RSA e AES

Obs: As chaves publica e privada do RSA podem ser encontradas na pasta "Keys".

No arquivo .properties da aplicação também é definido a URL/conexão do mongodb a ser utilizado.